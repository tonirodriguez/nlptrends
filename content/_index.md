---
title: Tendencias en Procesamiento del Lenguaje Natural (NLP)
description: Tendencias en Procesamiento del Lenguaje Natural (NLP).
---

Hoy en día es difícil encontrar un campo que esté progresando más rápido que el Procesamiento del Lenguaje Natural (NLP). El salto que se ha producido en este campo desde que se extendió el Deep Learning y más concretamente desde la aparición de las arquitecturas basadas en Transformers no tiene igual. Mensualmente se producen cientos de papers y en este blog vamos a intentar analizar las principales tendencias.
